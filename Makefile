MARIADB_CONTAINER=mariadb
SHELL=bash

.DEFAULT_GOAL=up

# Lift up the dev. environment
up:
	docker-compose -f docker-compose.yml up --build

# Take down the dev. environment
down:
	docker-compose down 

# Take down the dev. environment and remove containers, images
fdown:
	docker-compose down --rmi all

# Connect to the shell of the container of the mariadb
dshell:
	docker exec -it ${MARIADB_CONTAINER} ${SHELL}
