﻿using System;
using System.Diagnostics;
using MySql.Data.MySqlClient;

namespace sort
{
    class Program
    {
        static void Main(string[] args)
        {
            string connStr = "server=localhost;user=root;database=sort;port=3306;password=root";
            MySqlConnection conn = new MySqlConnection(connStr);
            try
            {
                conn.Open();
                // var ids = new int[] { 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27 };
                var ids = new int[] { };

                // Read items from db and manually sort it - Bubble Sort
                IGetSortItemsByIdsQuery getSortItemsByIdsQuery = new GetSortItemsByIdsQuery(conn);
                var timer = Stopwatch.StartNew();
                var items = getSortItemsByIdsQuery.Execute(ids);
                SortAlgorithms.BubbleSort(items);
                timer.Stop();
                Console.WriteLine($"Duration of manual sort(BubbleSort): {timer.ElapsedMilliseconds} ms.");
                // Read items from db and manually sort it - Quick Sort
                timer.Reset();
                timer.Start();
                var items2 = getSortItemsByIdsQuery.Execute(ids);
                SortAlgorithms.QuickSort(items2, 0, items2.Length - 1);
                timer.Stop();
                Console.WriteLine($"Duration of manual sort(QuickSort): {timer.ElapsedMilliseconds} ms.");

                // update the retrieved items with the rank
                timer.Reset();
                timer.Start();
                IUpdateSortItemCommand updateSortItemCommand = new UpdateSortItemCommand(conn);
                IUpdateSortItemsCommand updateSortItemsCommand =
                    new UpdateSortItemsCommand(updateSortItemCommand);
                updateSortItemsCommand.Execute(items);
                Console.WriteLine($"Duration of updating the rang: {timer.ElapsedMilliseconds} ms.");

                // retrieve the already sorted items from db
                timer.Reset();
                timer.Start();
                var items3 = getSortItemsByIdsQuery.Execute(ids, true);
                timer.Stop();
                Console.WriteLine($"Duration of db sort: {timer.ElapsedMilliseconds} ms.");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            conn.Close();
        }
    }
}
