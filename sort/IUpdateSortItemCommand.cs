namespace sort
{
    public interface IUpdateSortItemCommand
    {
        void Execute(SortItem _item);
    }
}
