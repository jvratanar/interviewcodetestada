namespace sort
{
    public interface IGetSortItemsByIdsQuery
    {
        SortItem[] Execute(int[] _ids, bool _dbSort = false);
    }
}
