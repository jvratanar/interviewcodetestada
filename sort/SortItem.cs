namespace sort
{
    public class SortItem
    {
        public int Id { get; set; }
        public string Value { get; set; }

        public int Rang { get; set; }
    }
}
