using System;
using System.Collections.Generic;
using System.Linq;
using MySql.Data.MySqlClient;

namespace sort
{
    public class GetSortItemsByIdsQuery : IGetSortItemsByIdsQuery
    {
        private MySqlConnection Connection { get; set; }

        public GetSortItemsByIdsQuery(MySqlConnection conn)
        {
            this.Connection = conn;
        }


        public SortItem[] Execute(int[] _ids, bool _dbSort = false)
        {
            try
            {
                // string sql = "SELECT id, value FROM sort_items WHERE id IN ({0})";
                string sql = "SELECT SQL_NO_CACHE id, value FROM sort_items";
                string[] paramNames = new string[] { };
                if (_ids != null && _ids.Length > 0)
                {
                    sql += " WHERE id IN ({0})";
                    paramNames = _ids.Select((s, i) => "@p" + i).ToArray();
                    string inClause = string.Join(',', paramNames);
                    sql = string.Format(sql, inClause);
                }

                if (_dbSort)
                    sql += " ORDER BY value ASC";

                // string[] paramNames = _ids.Select((s, i) => "@p" + i).ToArray();
                // string inClause = string.Join(',', paramNames);
                MySqlCommand cmd = new MySqlCommand(sql, this.Connection);
                for (int i = 0; i < paramNames.Length; i++)
                {
                    cmd.Parameters.AddWithValue(paramNames[i], _ids[i]);
                }

                MySqlDataReader rdr = cmd.ExecuteReader();
                var result = new List<SortItem>();
                while (rdr.Read())
                {
                    result.Add(new SortItem
                    {
                        Id = (int)rdr[0],
                        Value = (string)rdr[1]
                    });
                }
                rdr.Close();

                return result.ToArray();
            }
            catch (Exception e)
            {
                Console.WriteLine($"DB Error occured {e.StackTrace}");
            }

            return null;
        }
    }
}