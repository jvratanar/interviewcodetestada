using MySql.Data.MySqlClient;

namespace sort
{
    public class UpdateSortItemsCommand : IUpdateSortItemsCommand
    {
        private IUpdateSortItemCommand updSortItemCommand { get; set; }

        public UpdateSortItemsCommand(IUpdateSortItemCommand _updSortItemCommand)
        {
            this.updSortItemCommand = _updSortItemCommand;
        }


        public void Execute(SortItem[] _items)
        {
            for (int i = 0; i < _items.Length; i++)
            {
                var item = _items[i];
                item.Rang = i;
                this.updSortItemCommand.Execute(item);
            }
        }
    }
}