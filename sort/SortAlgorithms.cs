namespace sort
{
    public class SortAlgorithms
    {
        public static void BubbleSort(SortItem[] _items)
        {
            for (int i = 0; i < _items.Length - 1; i++)
            {
                for (int j = 0; j < _items.Length - i - 1; j++)
                {
                    if (_items[j].Value.CompareTo(_items[j + 1].Value) > 0)
                    {
                        var tmp = _items[j + 1];
                        _items[j + 1] = _items[j];
                        _items[j] = tmp;
                    }
                }
            }
        }

        public static void QuickSort(SortItem[] _items, int _left, int _right)
        {
            if (_right - _left <= 0)
                return;

            int partitionIndex = partition(_items, _left, _right);
            QuickSort(_items, _left, partitionIndex - 1);
            QuickSort(_items, partitionIndex + 1, _right);
        }

        private static int partition(SortItem[] _items, int _left, int _right)
        {
            SortItem pivot = _items[_right];
            int i = _left - 1;

            for (int j = _left; j <= _right; j++)
            {
                if (_items[j].Value.CompareTo(pivot.Value) < 0)
                {
                    i++;
                    swap(_items, i, j);
                }
            }
            swap(_items, ++i, _right);

            return i;
        }

        private static void swap(SortItem[] _item, int _i, int _j)
        {
            var temp = _item[_i];
            _item[_i] = _item[_j];
            _item[_j] = temp;
        }
    }
}