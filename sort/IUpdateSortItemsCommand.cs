namespace sort
{
    interface IUpdateSortItemsCommand
    {
        void Execute(SortItem[] _items);
    }
}