using System;
using System.Collections.Generic;
using System.Linq;
using MySql.Data.MySqlClient;

namespace sort
{
    public class UpdateSortItemCommand : IUpdateSortItemCommand
    {
        private MySqlConnection Connection { get; set; }

        public UpdateSortItemCommand(MySqlConnection conn)
        {
            this.Connection = conn;
        }


        public void Execute(SortItem _item)
        {
            try
            {
                string sql = "UPDATE sort_items SET rang = @Rang WHERE id = @Id";
                MySqlCommand cmd = new MySqlCommand(sql, this.Connection);
                cmd.Parameters.AddWithValue("@Id", _item.Id);
                cmd.Parameters.AddWithValue("@Rang", _item.Rang);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Console.WriteLine($"DB Error occured {e.StackTrace}");
            }
        }
    }
}