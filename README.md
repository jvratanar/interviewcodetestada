# Sorting Algorithms

## The program

1. Reads the items from database
2. Sorts the items manually and displays the time it took to perform actions 1. and 2.
   Current Sorting algorithm is **Bubble sort** O(n^2).
3. Updates the rang column of items in database which reflects the order after the
   sort and display the time taken to perform this step.
4. Reads the items from the database but this time the sorting is done via `ORDER BY`
5. Displays the elapsed time for the 4. step

## Prerequisites

- Docker
- Make
- dotnet cli

## Installation and Usage

Clone the repository.

```Bash
git clone https://gitlab.com/jvratanar/interviewcodetestada.git
```

Move to specific folder and lift up mysql and phpmyadmin containers

```Bash
cd interviewcodetestada && make
```

Restore nuget packages

```Bash
cd sort && dotnet restore
```

Run the program.

```Bash
dotnet bin/Debug/netcoreapp2.2/sort.dll
```
